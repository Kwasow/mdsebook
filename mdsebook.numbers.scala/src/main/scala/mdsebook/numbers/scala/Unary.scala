// (c) mdsebook, wasowski, tberger
// run as 'sbt run' and select  MainUnary as the main class

package mdsebook.numbers.scala

object Unary {

  case class Number (n: Int) {
    def I = Number (n+1)
    def I (i :Number) :Number = Number(n+2)
    override def toString = n.toString
  }

  object I extends Number(1)
}


object Unary2 { // vesion with glued digits

  case class Number (n: Int) {

    def I (i :Number) = Number (n + i.n + 1)
    def II (i :Number) = Number (n + i.n + 2)
    def III (i :Number) = Number (n + i.n + 3)
    def IIIII (i :Number) = Number (n + i.n + 5)

    def I = Number (n+1)
    def II = Number (n+2)
    def III = Number (n+3)
    def IIIII = Number (n+5)

    override def toString = n.toString
  }

  object I extends Number (1)
  object II extends Number (2)
  object III extends Number (3)
  object IIIII extends Number (5)
}


object MainUnary extends App {

  import scala.language.postfixOps

  { import Unary._

    // this is easier to support
    val l1 = List (I, I.I, I.I.I, I.I.I.I, I.I.I.I.I)
    l1 foreach {println _}

    // this is slightly cooler
    println
    val l2 = List (I, I I, I I I, I I I I, I I I I I)
    l2 foreach {println _}
  }

  { import Unary2._
    println
    val l3 = List (II II, II III, III III, IIIII IIIII, I III I, I II I, I)
    l3 foreach {println _}
  }
}

