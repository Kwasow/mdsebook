// (c) mddbook, wasowski, tberger
// This is the main driver for printer constraints

package mdsebook.sql.scala

import mdsebook.scala.EMFScala._
import mdsebook.sql.Model
import mdsebook.sql.SqlPackage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

object Main extends App {

  // Register a resource factory for XMI files

  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)

  // Register the the meta-model packages (quite a few in this exercise, impure)

  SqlPackage.eINSTANCE.eClass

  // load the XMI file (create one!)
  val uri: URI = URI.createURI ("../mdsebook.sql/test-files/test-00.xmi") // <-- change file name here
  val resource: Resource = (new ResourceSetImpl).getResource (uri, true)

  // http://download.eclipse.org/modeling/emf/emf/javadoc/2.11/org/eclipse/emf/ecore/util/EcoreUtil.html#getAllProperContents%28org.eclipse.emf.ecore.resource.Resource,%20boolean%29
  val content :Iterator[EObject] =
    EcoreUtil.getAllProperContents[EObject] (resource, false)

  if (content.forall { eo => Constraints.invariants.forall (_._2 check eo) })
    println ("All constraints are satisfied!")
  else
    println ("Some constraint is violated!")

}

