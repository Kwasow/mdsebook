// (c) mdsebook, wasowski, tberger

package mdsebook.sql.scala

import scala.collection.JavaConverters._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.sql._

object Constraints {

	val invariants = List[(String,Constraint)] (

    "Empty constraint" -> inv[Model] { self => true }

  )

}
