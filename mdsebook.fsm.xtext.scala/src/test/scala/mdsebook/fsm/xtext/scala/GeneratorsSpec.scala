// (c) mddbook, wasowski, tberger
package mdsebook.fsm.xtext.scala

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import scala.util.Try

class GeneratorsSpec extends AnyFreeSpec with Matchers {

  "Integration test" - {

    "Xtext parsers and generators to Java and Dot run without throwing exceptions" - {
      "CoffeeMachine.fsm" in {
        Try(new Generators ("../mdsebook.fsm/test-files/CoffeeMachine.fsm"))
          .shouldBe ('success)
      }

      "Complete.fsm" in {
        Try(new Generators ("../mdsebook.fsm/test-files/Complete.fsm"))
          .shouldBe ('success)
      }
    }
  }
}
