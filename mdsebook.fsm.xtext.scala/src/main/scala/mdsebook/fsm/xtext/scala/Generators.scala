// (c) mdsebook, wasowski, berger

// An example showing how to runs the code generators (transforms)
// outside Eclipse.
//
// To run this example run mdsebook.fsm.xtext.scala.Main

package mdsebook.fsm.xtext.scala

import java.io.PrintWriter
import scala.jdk.CollectionConverters._

import mdsebook.fsm.scala.transforms.{Fsm2Java, Fsm2Dot}
import mdsebook.fsm.xtext.FsmStandaloneSetup
import mdsebook.fsm.{FsmPackage, FiniteStateMachine, Model}
import mdsebook.scala.EMFScala._
import org.eclipse.xtext.resource.XtextResourceSet

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl

class Generators (val instanceFileName: String) {

  println (s"Generating Java and Dot code from ${instanceFileName}")

  // Register our meta-model package for abstract syntax
  FsmPackage.eINSTANCE.eClass

  // Register a suitable resource factory for FSM files (generated by Xtext)
  // The "Fsm" prefix is the name of our language
  // it needs to change to refer to code generated for your language
  val injector = new FsmStandaloneSetup().createInjectorAndDoEMFRegistration

  // we are loading our file here
  val uri = URI.createURI(instanceFileName)
  val resourceSet = injector.getInstance(classOf[XtextResourceSet])
  var resource = resourceSet.getResource(uri, true);

  // The call to get(0) below gives you the first model root.
  // If you open a directory instead of a file,
  // you can iterate over all models in it,
  // by changing 0 to a suitable index
  val m: FiniteStateMachine = resource.getContents.get(0).asInstanceOf[Model].getMachines.get(0)

  val name = "src-gen/" + m.getName.capitalize

  println (s"|-Starting Java.")
  new PrintWriter(name + ".java") { write(Fsm2Java compileToJava m); close }

  println (s"|-Starting Dot.")
  new PrintWriter(name + ".dot") { write(Fsm2Dot compileToDot m); close }

  println (s"|-Done.")

}

