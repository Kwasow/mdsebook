// (c) mdsebook, wasowski, tberger
// run as 'sbt run'
package mdsebook.dateformatter.scala.internal

import scala.language.postfixOps

object DSL {

  case class Date (day: Int, month: Int, year: Int)

  case class Formatter (fmt: Date => String) {
    def format (d: Date): String = fmt (d)
    def apply (that: Formatter): Formatter =
      Formatter { d => (this format d) + (that format d) }
    def dd   = apply (DSL.dd)
    def ddth = apply (DSL.ddth)
    def mm   = apply (DSL.mm)
    def mmm  = apply (DSL.mmm)
    def yy   = apply (DSL.yy)
    def yyyy = apply (DSL.yyyy)
    def -    = apply (DSL.-)
    def /    = apply (DSL./)
  }

  val shortMonths = Array ("Jan", "Feb", "Mar", "Apr", "May", "Jun",
                           "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

  val dd   = Formatter { d => "%02d" format (d.day) }
  val ddth = Formatter { _.month match { case 1 => "1st"
                                        case 2 => "2nd"
                                        case 3 => "3rd"
                                        case n if n >=4 => s"${n}th" } }

  val mm   = Formatter { d => "%02d" format (d.month) }
  val mmm  = Formatter { d => shortMonths (d.month - 1) }
  val yy   = Formatter { d => "%02d" format (d.year % 100) }
  val yyyy = Formatter { _.year.toString }

  val - = Formatter { _ => "-" }
  val / = Formatter { _ => "/" }

  implicit def S (s: String) :Formatter = Formatter (_ => s)
}


