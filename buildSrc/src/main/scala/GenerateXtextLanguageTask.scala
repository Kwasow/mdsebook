// (c) mdsebook, wasowski, tberger
//
// A gradle task implemented in Scala to generate code from xtext models

import scala.jdk.CollectionConverters._
import org.gradle.api.tasks.{JavaExec, InputFile, OutputDirectory, SourceSetContainer}
import javax.inject.Inject

class GenerateXtextLanguageTask (
  xtextFile: String, 
  mwe2File: String, 
  foo: Unit
) extends JavaExec {

  val root = this.getProject.getRootProject.getProjectDir().toString + "/"

  setMain ("org.eclipse.emf.mwe2.launch.runtime.Mwe2Launcher")
  setClasspath (this.getProject.getConfigurations.getByName ("compile"))
  setArgs (List(mwe2File, "-p", s"rootPath=${root}").asJava)

  val outputPath =  "src/main/xtext-gen"

  println (s"[mdsebook] GenerateXtextLanguage " +
           s"for '${xtextFile}' with '${mwe2File}' " +
           s"into '${outputPath}'")

  @InputFile
  def getXtextFile () = xtextFile

  @InputFile
  def getMwe2File () = mwe2File

  @OutputDirectory
  def getOutputPath () = outputPath

  getProject
    .getTasksByName ("generateXtext", false)
    .iterator
    .next
    .dependsOn (this)

  val sourceSets: SourceSetContainer = 
    getProject
      .getProperties
      .get ("sourceSets")
      .asInstanceOf[SourceSetContainer]

  sourceSets
    .getByName ("main")
    .getJava
    .srcDir ("src/main/java")
    .srcDir ("src/main/xtext-gen")
    .srcDir ("src/main/xtend-gen")

  sourceSets
    .getByName ("test")
    .getJava
    .srcDir ("src/test/java")
    .srcDir ("src/test/xtext-gen")
    .srcDir ("src/test/xtend-gen")

  // Gradle requires that the constructor is annotated with Inject, so we make
  // the constructor here, and add a fake parameter to the default class
  // constructor, to distinguish the injected and the regular one.
  @Inject
  def this(xtextFile: String, mwe2File: String) = 
    this(xtextFile, mwe2File, ())

}
