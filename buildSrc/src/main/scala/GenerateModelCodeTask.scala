// (c) mdsebook, wasowski, tberger
//
// A gradle task implemented in Scala, as Groovy had trouble finding
// GenerateModelCode (scala)

import org.gradle.api._
import org.gradle.api.tasks._
import javax.inject.Inject

// the genmodel should be a path relative to the root of the platform (the book
// project)
class GenerateModelCodeTask (genmodel: String, foo: Unit) extends DefaultTask {

  // Gradle requires that the constructor is annotated with Inject, so we make
  // the constructor here, and add a fake parameter to the default class
  // constructor, to distinguish the injected and the regular one.
  @Inject
  def this(genmodel: String) = this(genmodel, ())

  val root = getProject.getRootProject.getProjectDir.toString + "/"
  val generator = mdsebook.cmd.GenerateModelCode (genmodel, root)
  val genmodelPath = this.getProject.file (root + generator.genmodelPath)
  val ecorePath = getProject
    .file (root+generator.genmodelPath)
    .getParentFile
    .toString + "/" + generator.genmodel.getForeignModel.get(0)
  val codePath = this.getProject.file (root + generator.codePath)

  println (s"[mdsebook] GenerateModelCodeTask " +
           s"for '${genmodelPath}' " + 
           s"into '${codePath}'")

  @InputFile
  def getGenmodelPath () = genmodelPath
  @InputFile
  def getEcorePath () = ecorePath

  @OutputDirectory
  def getCodePath () = codePath

  @TaskAction
  def taskAction () = generator.clean.generate
}
