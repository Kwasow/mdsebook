// (c) mdsebook, wasowski, tberger
package mdsebook.cmd

object Main extends App {

  args match {
    case Array(genmodel) =>
      GenerateModelCode(genmodel).clean.generate
    case Array(genmodel, platform) =>
      GenerateModelCode(genmodel,platform).clean.generate
    case _ =>
      GenerateModelCode.log("""
              |Usage:   GenerateModelCode genmodel_uri
              |Example: ./GenerateModelCode mdsebook.fsm/model/fsm.genmodel
              |
              |The invocation should be done from within an Eclipse Workspace,
              |probably in a root directory of a project in the same workspace
              |where the platform URI is referring to.
              |""".stripMargin)
  }
}

