/* Copyright 2020 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   Credits: The file contents is inspired by 
   https://www.eclipse.org/forums/index.php/t/1075893/ */

package mdsebook.cmd

import scala.util.Try
import org.eclipse.emf.codegen.ecore.genmodel.GenModel
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.codegen.ecore.generator.Generator

import GenerateModelCode._

case class GenerateModelCode (
  genmodelPath: String,        // relative to platform root
  platformPath: String = ".."  // relative to current directory
) {

  def genmodelURI = s"platform:/resource/${genmodelPath}"

  // Primary constructor (basically standalone setup)

  new org.eclipse.emf.mwe.utils.StandaloneSetup()
    .setPlatformUri (platformPath)
  log (s"Standalone setup completed, platformPath=${platformPath}")

  // A magic incantation to register genmodel's meta-model
  // as suggested there: https://www.eclipse.org/forums/index.php/t/126755/
  org.eclipse.emf.codegen.ecore.genmodel.GenModelPackage.eINSTANCE.eClass
  log ("Ecore genmodel package registered.")
  log (s"Loading genmodel: ${genmodelPath}")

  val uri: URI = URI.createURI (genmodelURI)
  val resource: Resource = (new ResourceSetImpl).getResource (uri, true)
  val genmodel: GenModel =
     EcoreUtil.getAllProperContents[GenModel] (resource, false).next
  log (s"Genmodel loaded: ${genmodelPath}")

  def firstLower (s: String): String = s(0).toLower.toString + s.drop (1)

  val codePath =
     genmodel.getModelDirectory +"/"+
     genmodel.getGenPackages.get(0).getBasePackage.replaceAllLiterally (".","/") +"/"+
     firstLower (genmodel.getModelName)

  def codeURI = s"platform:/resource/${codePath}"

  // Functions

  def clean: GenerateModelCode = {
    // If not there, then already empty (ignore the exception)
    Try (new org.eclipse.emf.mwe.utils.DirectoryCleaner()
        .cleanFolder (platformPath + codePath))
    log (s"Target directory cleaned: ${codePath}")
    this
  }

  def generate: Unit = {
    val generator = new org.eclipse.emf.mwe2.ecore.EcoreGenerator
    generator.setGenModel (genmodelURI)
    generator.addSrcPath (codeURI)
    generator.invoke (null) // null fine as the generator ignores the arg
    log ("Code generated")
  }

}

object GenerateModelCode {

  def log (s: String) = {
    println (s"mdsebook.cmd.GenerateModelCode: $s")
    Console.flush
  }
}
