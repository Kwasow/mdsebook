/* Copyright 2020 Andrzej Wasowski and Thorsten Berger

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

   Credits: The file contents is inspired by 
   https://www.eclipse.org/forums/index.php/t/1075893/ */

package mdsebook.cmd

import org.scalatest.matchers.should.Matchers
import org.scalatest.freespec.AnyFreeSpec
import scala.util.Try
import java.io.File

class GenerateModelCodeSpec extends AnyFreeSpec with Matchers {

  val fsmJava = new File("../mdsebook.fsm/src/mdsebook/fsm/FiniteStateMachine.java")
  val fsmGenmodel = "mdsebook.fsm/model/fsm.genmodel"
  val platform = ".."
  val generator = GenerateModelCode (fsmGenmodel,platform)

  "generate FSM model code and check that files are indeed produced" - {

    "cleaning directories works" in {
      fsmJava.getParentFile().mkdirs()
      fsmJava.createNewFile()
      fsmJava should exist
      generator.clean
      fsmJava shouldNot exist
    }

    "generator creates files in src/mdsebook/fsm/" in {
      Try(generator.clean.generate) shouldBe Symbol("success")
      fsmJava should exist
      generator.clean // don't affect the main script by build tests
      fsmJava shouldNot exist
    }

  }

}
