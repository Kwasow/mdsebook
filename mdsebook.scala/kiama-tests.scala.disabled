// on case classes the rewriter works out of the box
//
// trying without the Rewritable interface does not work. I got to the rewriter
// failing silently (like for Ecore!)
//
// now trying with the Rewritable.

trait Exp1
case class Plus (l: Exp1, r: Exp1) extends Exp1
case class Sub  (l: Exp1, r: Exp1) extends Exp1
case class Const (value: Int) extends Exp1

val g = Plus (Plus (Const (1), Const (2)), Const (3))

val eval1 = rule[Exp1] {
  case Plus (Const (l), Const(r)) => Const (l+r)
  case Sub  (Const (l), Const(r)) => Const (l-r)
}


import scala.collection.immutable.Seq
abstract trait Exp2 extends Rewritable

class Plus2 extends Exp2 {

  def arity : Int = 2
  def deconstruct : Seq[Any] = List (l,r)

  def reconstruct (components : Seq[Any]) : Plus2 =
    new Plus2 (components(0).asInstanceOf[Exp2],
      components(1).asInstanceOf[Exp2])

  var l: Exp2 = null
  var r: Exp2 = null
  def this (a: Exp2, b: Exp2) = { this(); l = a; r = b }
  override def toString = s"($l) + ($r)"
}

class Sub2 extends Exp2 {
  def arity : Int = 2
  def deconstruct : Seq[Any] = List(l,r)
  def reconstruct (components : Seq[Any]) : Sub2 =
    new Sub2 (components(0).asInstanceOf[Exp2],
      components(1).asInstanceOf[Exp2])

  var l: Exp2  = null
  var r: Exp2  = null
  def this (a: Exp2, b: Exp2) = { this(); l = a; r = b }
  override def toString = s"($l) - ($r)"
}

class Const2 extends Exp2 {
  def arity : Int = 1
  def deconstruct : Seq[Any] = List(value)
  def reconstruct (components: Seq[Any]) :Const2 = components match {
    case Seq(n :Int) => new Const2(n)
    case _ => illegalArgs ("Const2", "_?_", components)
  }
  var value: Int = 0
  def this (n: Int) = { this(); value = n }
  override def toString = value.toString
}

val h = new Plus2 ( new Plus2 (new Sub2 (new Const2(2000), new Const2(1000)),
                               new Const2 (2)),
                    new Const2 (3))

val eval2 = rule[Exp2] {
  case x: Plus2 if x.l.isInstanceOf[Const2] && x.r.isInstanceOf[Const2]
    => new Const2 (x.l.asInstanceOf[Const2].value + x.r.asInstanceOf[Const2].value)
  case x: Sub2 if x.l.isInstanceOf[Const2] && x.r.isInstanceOf[Const2]
    => new Const2 (x.l.asInstanceOf[Const2].value - x.r.asInstanceOf[Const2].value)
 }
