/**
 */
package mdsebook.testModels.arith;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see mdsebook.testModels.arith.ArithPackage
 * @generated
 */
public interface ArithFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArithFactory eINSTANCE = mdsebook.testModels.arith.impl.ArithFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Plus Exp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plus Exp</em>'.
	 * @generated
	 */
	PlusExp createPlusExp();

	/**
	 * Returns a new object of class '<em>Sub Exp</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sub Exp</em>'.
	 * @generated
	 */
	SubExp createSubExp();

	/**
	 * Returns a new object of class '<em>Const Int</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Const Int</em>'.
	 * @generated
	 */
	ConstInt createConstInt();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ArithPackage getArithPackage();

} //ArithFactory
