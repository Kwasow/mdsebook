/**
 */
package mdsebook.testModels.arith;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mdsebook.testModels.arith.ArithFactory
 * @model kind="package"
 * @generated
 */
public interface ArithPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "arith";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.mdsebook.org/mdsebook.testModels.arith";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mdsebook.testModels.arith";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ArithPackage eINSTANCE = mdsebook.testModels.arith.impl.ArithPackageImpl.init();

	/**
	 * The meta object id for the '{@link mdsebook.testModels.arith.impl.ExpImpl <em>Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.testModels.arith.impl.ExpImpl
	 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getExp()
	 * @generated
	 */
	int EXP = 1;

	/**
	 * The number of structural features of the '<em>Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXP_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link mdsebook.testModels.arith.impl.BinExpImpl <em>Bin Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.testModels.arith.impl.BinExpImpl
	 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getBinExp()
	 * @generated
	 */
	int BIN_EXP = 3;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__LEFT = EXP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP__RIGHT = EXP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Bin Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP_FEATURE_COUNT = EXP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Bin Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIN_EXP_OPERATION_COUNT = EXP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.testModels.arith.impl.PlusExpImpl <em>Plus Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.testModels.arith.impl.PlusExpImpl
	 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getPlusExp()
	 * @generated
	 */
	int PLUS_EXP = 0;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUS_EXP__LEFT = BIN_EXP__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUS_EXP__RIGHT = BIN_EXP__RIGHT;

	/**
	 * The number of structural features of the '<em>Plus Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUS_EXP_FEATURE_COUNT = BIN_EXP_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Plus Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUS_EXP_OPERATION_COUNT = BIN_EXP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.testModels.arith.impl.SubExpImpl <em>Sub Exp</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.testModels.arith.impl.SubExpImpl
	 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getSubExp()
	 * @generated
	 */
	int SUB_EXP = 2;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_EXP__LEFT = BIN_EXP__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_EXP__RIGHT = BIN_EXP__RIGHT;

	/**
	 * The number of structural features of the '<em>Sub Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_EXP_FEATURE_COUNT = BIN_EXP_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sub Exp</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUB_EXP_OPERATION_COUNT = BIN_EXP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link mdsebook.testModels.arith.impl.ConstIntImpl <em>Const Int</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mdsebook.testModels.arith.impl.ConstIntImpl
	 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getConstInt()
	 * @generated
	 */
	int CONST_INT = 4;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_INT__VALUE = EXP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Const Int</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_INT_FEATURE_COUNT = EXP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Const Int</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONST_INT_OPERATION_COUNT = EXP_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link mdsebook.testModels.arith.PlusExp <em>Plus Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plus Exp</em>'.
	 * @see mdsebook.testModels.arith.PlusExp
	 * @generated
	 */
	EClass getPlusExp();

	/**
	 * Returns the meta object for class '{@link mdsebook.testModels.arith.Exp <em>Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exp</em>'.
	 * @see mdsebook.testModels.arith.Exp
	 * @generated
	 */
	EClass getExp();

	/**
	 * Returns the meta object for class '{@link mdsebook.testModels.arith.SubExp <em>Sub Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sub Exp</em>'.
	 * @see mdsebook.testModels.arith.SubExp
	 * @generated
	 */
	EClass getSubExp();

	/**
	 * Returns the meta object for class '{@link mdsebook.testModels.arith.BinExp <em>Bin Exp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bin Exp</em>'.
	 * @see mdsebook.testModels.arith.BinExp
	 * @generated
	 */
	EClass getBinExp();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.testModels.arith.BinExp#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see mdsebook.testModels.arith.BinExp#getLeft()
	 * @see #getBinExp()
	 * @generated
	 */
	EReference getBinExp_Left();

	/**
	 * Returns the meta object for the containment reference '{@link mdsebook.testModels.arith.BinExp#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see mdsebook.testModels.arith.BinExp#getRight()
	 * @see #getBinExp()
	 * @generated
	 */
	EReference getBinExp_Right();

	/**
	 * Returns the meta object for class '{@link mdsebook.testModels.arith.ConstInt <em>Const Int</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Const Int</em>'.
	 * @see mdsebook.testModels.arith.ConstInt
	 * @generated
	 */
	EClass getConstInt();

	/**
	 * Returns the meta object for the attribute '{@link mdsebook.testModels.arith.ConstInt#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see mdsebook.testModels.arith.ConstInt#getValue()
	 * @see #getConstInt()
	 * @generated
	 */
	EAttribute getConstInt_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ArithFactory getArithFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mdsebook.testModels.arith.impl.PlusExpImpl <em>Plus Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.testModels.arith.impl.PlusExpImpl
		 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getPlusExp()
		 * @generated
		 */
		EClass PLUS_EXP = eINSTANCE.getPlusExp();

		/**
		 * The meta object literal for the '{@link mdsebook.testModels.arith.impl.ExpImpl <em>Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.testModels.arith.impl.ExpImpl
		 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getExp()
		 * @generated
		 */
		EClass EXP = eINSTANCE.getExp();

		/**
		 * The meta object literal for the '{@link mdsebook.testModels.arith.impl.SubExpImpl <em>Sub Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.testModels.arith.impl.SubExpImpl
		 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getSubExp()
		 * @generated
		 */
		EClass SUB_EXP = eINSTANCE.getSubExp();

		/**
		 * The meta object literal for the '{@link mdsebook.testModels.arith.impl.BinExpImpl <em>Bin Exp</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.testModels.arith.impl.BinExpImpl
		 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getBinExp()
		 * @generated
		 */
		EClass BIN_EXP = eINSTANCE.getBinExp();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXP__LEFT = eINSTANCE.getBinExp_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BIN_EXP__RIGHT = eINSTANCE.getBinExp_Right();

		/**
		 * The meta object literal for the '{@link mdsebook.testModels.arith.impl.ConstIntImpl <em>Const Int</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mdsebook.testModels.arith.impl.ConstIntImpl
		 * @see mdsebook.testModels.arith.impl.ArithPackageImpl#getConstInt()
		 * @generated
		 */
		EClass CONST_INT = eINSTANCE.getConstInt();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONST_INT__VALUE = eINSTANCE.getConstInt_Value();

	}

} //ArithPackage
