/**
 */
package mdsebook.testModels.arith.impl;

import mdsebook.testModels.arith.ArithPackage;
import mdsebook.testModels.arith.PlusExp;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plus Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PlusExpImpl extends BinExpImpl implements PlusExp {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlusExpImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ArithPackage.Literals.PLUS_EXP;
	}

} //PlusExpImpl
