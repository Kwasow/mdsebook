/**
 */
package mdsebook.testModels.arith;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plus Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mdsebook.testModels.arith.ArithPackage#getPlusExp()
 * @model
 * @generated
 */
public interface PlusExp extends BinExp {
} // PlusExp
