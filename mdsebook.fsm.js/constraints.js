// Copyright 2020 Andrzej Wasowski and Thorsten Berger
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// This is a node.js + ecore.js example.
// To run, install nodejs, for instance:
// $ sudo apt install nodejs 
// All JavaScript dependencies are committed to the git repo otherwise.
// NB. Ecore.js works also in the browser, which might be useful for websites
// that use DSLs.

var Ecore = require('ecore/dist/ecore.xmi')
var fs = require('fs')

// Load the FSM meta-model from mdsebook.fsm
var resourceSet = Ecore.ResourceSet.create()
var resourceFsm = resourceSet.create({ uri: '../mdesbook.fsm/model/fsm.ecore' })
var sFsm    = fs.readFileSync('../mdsebook.fsm/model/fsm.ecore', 'utf8')
resourceFsm.parse (sFsm, Ecore.XMI)

// Register the meta-model of FSM in the registry
var packageFsm = resourceFsm.get ('contents').first()
Ecore.EPackage.Registry.register (packageFsm)

// The code below loads a handful test-cases and evaluates the constraint on
// them producing some error messages.

// A helper function to load fsm instances
function loadInstance (fname) 
{
  console.log (`Loading ${fname} ...`)
  var resource = resourceSet.create ({uri: fname})
  var s = fs.readFileSync(fname, 'utf8')
  resource.parse(s, Ecore.XMI)
  return resource.get ('contents').first ()
}

var C2 = m =>
  m.get('states').array().every ( s1 =>
    m.get('states').array().every ( s2 =>
      (s1!=s2) <= (s1.get('name')!=s2.get('name'))
    )
  )


function checkAll (fname) 
{
  var testCase = loadInstance  (`../mdsebook.fsm/test-files/${fname}.xmi`)
  var results = testCase.get('machines').map (C2)
  var result = results.every (t => t)

  if (result)
    console.log (`C2 is satisfied by all machines in ${fname}.xmi`)
  else
    for (i = 0; i < results.length; ++i)
      if (!results[i]) {
        var machine_i = testCase.get('machines').at(i).get('name')
        console.log (`C2 VIOLATED by machine '${machine_i}' in ${fname}.xmi`)
      }

  return result;
}

// File names we want to test (same as test cases for Scala)
var testModels = ['test-00', 'test-01', 'test-02', 'test-07', 
                  'test-09', 'test-11', 'test-04', 'test-14',
                  'test-08'];

// Just try C2 on all the above files
for (t of testModels) checkAll (t)
