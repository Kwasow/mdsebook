// (c) mdsebook, wasowski, tberger
package mdsebook.person.scala

import LazyList.empty

class Person ( name: String,
               parent: ()=>LazyList[Person],
               child: ()=>LazyList[Person] ) 
{
  def getName = name
  def getChild = child()
  def getParent = parent()
}

object Person {

  def apply (
    name: String, 
    parent: =>LazyList[Person] = empty, 
    child: =>LazyList[Person] = empty): Person = 
      new Person (name, ()=>parent, ()=>child)
}

