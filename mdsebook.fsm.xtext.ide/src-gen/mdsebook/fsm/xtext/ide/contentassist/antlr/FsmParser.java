/*
 * generated by Xtext 2.12.0
 */
package mdsebook.fsm.xtext.ide.contentassist.antlr;

import com.google.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import mdsebook.fsm.xtext.ide.contentassist.antlr.internal.InternalFsmParser;
import mdsebook.fsm.xtext.services.FsmGrammarAccess;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.ide.editor.contentassist.antlr.AbstractContentAssistParser;

public class FsmParser extends AbstractContentAssistParser {

	@Inject
	private FsmGrammarAccess grammarAccess;

	private Map<AbstractElement, String> nameMappings;

	@Override
	protected InternalFsmParser createParser() {
		InternalFsmParser result = new InternalFsmParser(null);
		result.setGrammarAccess(grammarAccess);
		return result;
	}

	@Override
	protected String getRuleName(AbstractElement element) {
		if (nameMappings == null) {
			nameMappings = new HashMap<AbstractElement, String>() {
				private static final long serialVersionUID = 1L;
				{
					put(grammarAccess.getFiniteStateMachineAccess().getGroup(), "rule__FiniteStateMachine__Group__0");
					put(grammarAccess.getFiniteStateMachineAccess().getGroup_2(), "rule__FiniteStateMachine__Group_2__0");
					put(grammarAccess.getStateAccess().getGroup(), "rule__State__Group__0");
					put(grammarAccess.getStateAccess().getGroup_2(), "rule__State__Group_2__0");
					put(grammarAccess.getTransitionAccess().getGroup(), "rule__Transition__Group__0");
					put(grammarAccess.getTransitionAccess().getGroup_3(), "rule__Transition__Group_3__0");
					put(grammarAccess.getFiniteStateMachineAccess().getNameAssignment_1(), "rule__FiniteStateMachine__NameAssignment_1");
					put(grammarAccess.getFiniteStateMachineAccess().getInitialAssignment_2_2(), "rule__FiniteStateMachine__InitialAssignment_2_2");
					put(grammarAccess.getFiniteStateMachineAccess().getStatesAssignment_2_3(), "rule__FiniteStateMachine__StatesAssignment_2_3");
					put(grammarAccess.getStateAccess().getNameAssignment_1(), "rule__State__NameAssignment_1");
					put(grammarAccess.getStateAccess().getLeavingTransitionsAssignment_2_1(), "rule__State__LeavingTransitionsAssignment_2_1");
					put(grammarAccess.getTransitionAccess().getInputAssignment_2(), "rule__Transition__InputAssignment_2");
					put(grammarAccess.getTransitionAccess().getOutputAssignment_3_1(), "rule__Transition__OutputAssignment_3_1");
					put(grammarAccess.getTransitionAccess().getTargetAssignment_6(), "rule__Transition__TargetAssignment_6");
				}
			};
		}
		return nameMappings.get(element);
	}
			
	@Override
	protected String[] getInitialHiddenTokens() {
		return new String[] { "RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT" };
	}

	public FsmGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(FsmGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
