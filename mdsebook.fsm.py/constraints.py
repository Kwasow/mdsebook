# Copyright 2020 Andrzej Wasowski and Thorsten Berger
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Install pyecore with: 'pip3 install pyecore"
# Run with: 'python3 constraints.py'

# Loosely inspired by
# https://modeling-languages.com/pyecore-python-eclipse-modeling-framework/ and
# the documentation of pyEcore (https://pyecore.readthedocs.io/en/latest/)

from pyecore.resources import ResourceSet, URI

# Load the fsm meta-model
resource_set = ResourceSet()
resource = resource_set.get_resource (URI ('../mdsebook.fsm/model/fsm.ecore'))
fsm_model = resource.contents[0] 

# Register fsm ast the meta-model (needed)
resource_set.metamodel_registry[fsm_model.nsURI] = fsm_model

# This is the example constraint in Python 3:

C2a = lambda m: all ( all ( 
            (s1 != s2) <= (s1.name != s2.name)
            for s2 in m.states )
        for s1 in m.states )

C2b = lambda m:  all ( 
        all ( s1.name != s2.name for s2 in m.states if s1 != s2)
        for s1 in m.states )

C2 = lambda m:  all ( s1.name != s2.name 
        for s1 in m.states for s2 in m.states if s1 != s2)


# The code below loads a handful test-cases and evaluates the constraint on
# them producing some error messages.



# A helper function to load fsm instances
def loadInstance (fname):
    print (f"Loading '{fname}'...")
    resource = resource_set.get_resource (URI (fname))
    return resource.contents[0]


# Check constraint C2 on all machines in the file designated by fname
def checkAll (fname):

    test_case = loadInstance  (f"../mdsebook.fsm/test-files/{fname}.xmi")
    results = [ C2(m) for m in test_case.machines ]

    if all (results):
        print (f"C2 is satisfied by all machines in {fname}.xmi")
    else:
        for i in range (len(results)):
            if not (results[i]):
                print (f"C2 VIOLATED by machine '{test_case.machines[i].name}' in {fname}.xmi")

    return all (results)



# File names we want to test (same as test cases for Scala)
test_models = ['test-00', 'test-01', 'test-02', 'test-07', 'test-09',
        'test-11', 'test-04', 'test-14', 'test-08']


# Just  try the constraint C2 on all the above files
for t in test_models:
    checkAll (t)
