// (c) mdsebook, wasowski, tberger

package mdsebook.robot.scala

import scala.collection.JavaConverters._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.robot._

object Constraints {

  // All reactions in the same mode should have distinct trigger events 

  val nonOverlappingTriggers = inv[Mode] { self => 
      val triggers = self.getReactions.asScala map { _.getTrigger }
      triggers.toSet.size == triggers.size }
  
  // Every mode has no submodes, or has an initial submode
  
  val allModesInitialized = inv[Mode] { self => 
    (!self.getModes.isEmpty) implies (self.getModes.asScala.exists {_.isInitial}) }
  
  val invariants: List[Constraint] = List (nonOverlappingTriggers, allModesInitialized)

}
