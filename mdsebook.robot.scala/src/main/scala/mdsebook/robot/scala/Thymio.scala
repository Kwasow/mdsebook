// (c) mdsebook, wasowski, berger
// This code is purposefully not made purely functional, to follow
// the programming style of Java ROS as closely as possible

package mdsebook.robot.scala

import org.ros.node.ConnectedNode
import org.ros.node.topic.Publisher
import org.ros.node.topic.Subscriber

import geometry_msgs.Twist
import sensor_msgs.LaserScan

class Thymio (cn: ConnectedNode) {

  private var proximity_laser: Subscriber[LaserScan] = null
  private var cmd_vel: Publisher[Twist] = null

  def getProximityTopic: Subscriber[LaserScan] = proximity_laser

  cmd_vel = cn newPublisher ("/cmd_vel", Twist._TYPE)
  proximity_laser = cn newSubscriber ("/proximity/laser", LaserScan._TYPE)

  def randomRotation: Thymio = {

    val msg: geometry_msgs.Twist = cmd_vel.newMessage
    msg.getAngular setZ (if (scala.util.Random.nextBoolean) -1 else 1)
    cmd_vel publish msg
    Thread.sleep(400 * scala.util.Random.nextInt(4)+1)

    msg.getAngular setZ 0
    cmd_vel publish msg
    this
  }

  def engage: Thymio = {
    val msg = cmd_vel.newMessage
    msg.getLinear setX 0.02
    cmd_vel publish msg
    Thread sleep 300 // give us a visual experience that we indeed tried
    this
  }

  def stop: Thymio = {
    val msg = cmd_vel.newMessage
    msg.getLinear setX 0
    cmd_vel publish msg
    Thread sleep 300
    this
  }

  def backOff: Thymio = {
    val msg = cmd_vel.newMessage
    msg.getLinear setX -0.02
    cmd_vel publish msg
    Thread sleep 800
    stop
    this
  }
}