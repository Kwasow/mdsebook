// (c) mdsebook, wasowski, tberger
//
// There are some difficulties in getting ParseHelp to work with scalatest and
// scala, so this file provides similar functionality using implicits and more
// natural scala style, so that we can unit tests grammars more easily.
package mdsebook.xtext.scala

import java.io.{InputStream, ByteArrayInputStream}

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.ISetup
import org.eclipse.xtext.resource.FileExtensionProvider
import org.eclipse.xtext.resource.XtextResourceSet

case class XtextScala (in: String) (setup: ISetup) {

  private val injector = setup.createInjectorAndDoEMFRegistration
  private val resourceSet = injector.getInstance (classOf[XtextResourceSet])
  private val extension =
    injector
      .getInstance (classOf[FileExtensionProvider])
      .getPrimaryFileExtension
  private val resource =
    resourceSet
      .createResource (URI.createURI("."+extension))

  def parse[T <: EObject]: Option[T] = {
    val input: InputStream = new ByteArrayInputStream (in.getBytes)
    resource.load (input, resourceSet.getLoadOptions)
    val contents = resource.getContents
    if (contents.isEmpty || !resource.getErrors.isEmpty)
      None
    else
      Some (contents.get(0).asInstanceOf[T])
  }
}



object XtextScala {

  implicit def toXtextScala (s: String) (implicit setup: ISetup): XtextScala =
    XtextScala (s) (setup)
}
