package mdsebook.fsm.xtend

import mdsebook.petrinet.PetrinetPackage
import mdsebook.fsm.Model

class ToPetriNetMain {

	def static void main (String[] args) {
		
		// register our meta-model package for abstract syntax
		mdsebook.fsm.FsmPackage.eINSTANCE.eClass
		PetrinetPackage.eINSTANCE.eClass
		
		// get our state machine from the abstract syntax (XMI file)
		val resource = XtendUtil.loadFromXMI( "../mdsebook.fsm/test-files/coffeemachine.xmi" )

		/* The call to get(0) below gives you the first model root. 
		// If you open a directory instead of a file, 
		// you can iterate over all models in it, 
		// by changing 0 to a suitable index */
		val fsmModel = resource.contents.get(0) as Model
		
		// we run the transformation, which changes m
		val petrinetModel = ToPetriNet.run( fsmModel )
		
		// save the resulting model
		XtendUtil.saveAsXMI( "xtend-gen/test-output.xmi", petrinetModel )
	}
	
}


