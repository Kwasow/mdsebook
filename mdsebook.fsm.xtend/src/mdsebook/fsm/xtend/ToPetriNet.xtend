// (c) mdsebook, wasowski, tberger
// An in-place transformation implemented in Xtend. Translates a finite state
// machine to a Petri net.  Run using ToPetriNetMain.xtend

package mdsebook.fsm.xtend

import mdsebook.fsm.FiniteStateMachine
import mdsebook.fsm.State

import org.eclipse.emf.common.util.EList
import mdsebook.fsm.Transition

class ToPetriNet {
	
	val static private pFactory = mdsebook.petrinet.PetrinetFactory.eINSTANCE

	def static mdsebook.petrinet.Place convertState( State s ){
		pFactory.createPlace => [
			name = s.name
			tokenNo = 0 ]
	}
	
	def static mdsebook.petrinet.Transition convertTransition( Transition t,
															    EList<mdsebook.petrinet.Place> p ){
		pFactory.createTransition => [
			input = t.input
			fromPlace += p.filter[ x | x.name == t.source.name ]
			toPlace += p.filter[ x | x.name == t.target.name ] ]
	}

	def static mdsebook.petrinet.PetriNet convertStateMachine( FiniteStateMachine fsm ){
		pFactory.createPetriNet => [
			name = fsm.name
			places += fsm.states.map[s | convertState( s ) ]
			transitions += fsm.states.map[ s | s.leavingTransitions ].flatten.
			               map[ s | convertTransition( s, places ) ]
			
			// add a token to the initial state
			places.findFirst[ name == fsm.initial.name ].tokenNo = 1
			
			// for each end state generate a transition that throws a token away
			fsm.endStates.forEach[ s | 
				val p = places.findFirst[ name == s.name ]
				val t = pFactory.createTransition => [ fromPlace += p; input = "" ]
				p.outgoingTransitions += t
				transitions += t ] ]
	}	

	def static mdsebook.petrinet.Model run( mdsebook.fsm.Model m ){
		pFactory.createModel => [
			petrinets += m.machines.map[ convertStateMachine ] ]
	}	
			
	private def static endStates( FiniteStateMachine m ){
		m.states.filter[ s | s.leavingTransitions.empty ]
	}
		
}
