// (c) mdsebook, wasowski, tberger

package mdsebook.pipes.scala

import scala.collection.JavaConverters._ // for natural access to EList
import mdsebook.scala.EMFScala._
import mdsebook.pipes._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[PipesModel] { m =>
      m.getNodes.asScala.filter { _.isInstanceOf[Source] }.size == 1 &&
        m.getNodes.asScala.filter { _.isInstanceOf[Sink] }.size == 1
    }

  )

}
