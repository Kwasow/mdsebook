// (c) mdsebook, wasowski, tberger
// A parser for Fsm implemented using Parboiled2
// An alternative to the xtext parser defined in
// mdsebook.fsm.xtext/src/main/java/mdsebook/fsm/xtext/Fsm.xtext

package mdsebook.fsm.scala

import org.parboiled2._
import mdsebook.fsm.scala.adt.Pure

case class FsmParser (val input: ParserInput) extends Parser {

  // Non-terminals, starting with the Start symbol

  def model: Rule1[Pure.Model] =
    rule { machineBlock.* ~ EOI ~> Model }

  def machineBlock: Rule1[Pure.FiniteStateMachine] =
    rule {
      "machine" ~ EString ~ BEGIN ~
        stateBlock.* ~
        initialDeclaration ~
        stateBlock.* ~
      END ~> FiniteStateMachine
    }

  def initialDeclaration: Rule1[String] =
    rule { "initial" ~ EString }

  def stateBlock: Rule1[StateTr] =
    rule {
      "state" ~ EString ~ ( BEGIN ~
        transition.* ~
      END ).? ~> StateTransitions
    }

  def transition: Rule1[Pure.Transition] =
    rule { inputClause ~ outputClause.? ~ targetClause ~> Transition }

  def inputClause: Rule1[String] =
    rule { "on" ~ "input".? ~ EString }

  def outputClause: Rule1[String] =
    rule { "output" ~ EString ~ "and" }

  def targetClause: Rule1[String] =
    rule { "go" ~ "to" ~ EString }

  def EString: Rule1[String] =
    rule { ID | STRING }


  // Terminals and whitespace ("lexing")

  def STRING: Rule1[String] =
    rule { WS.? ~ '"' ~ capture ((!'"' ~ ANY).*) ~ '"' ~ WS.? }

  val IDFirst: CharPredicate = CharPredicate.Alpha ++ "_"
  val IDSuffix: CharPredicate = CharPredicate.AlphaNum ++ "_"

  def ID: Rule1[String] =
    rule { WS.? ~ capture (IDFirst ~ IDSuffix.*) ~ WS.? }

  def BEGIN =
    rule { WS.? ~ '[' ~ WS.? }
  def END =
    rule { WS.? ~ ']' ~ WS.? }

  implicit def StringWS (s: String): Rule0 =
    rule { WS.? ~ str (s) ~ WS }

  def WS: Rule0 =
    rule { anyOf (" \n\t").+ }


  // Semantic actions (gathered here to keep the grammar clean)

  val Model: Seq[Pure.FiniteStateMachine] => Pure.Model =
    machines => Pure.Model ("", machines.toList)

  type StateTr = (String, List[Pure.Transition])

  val FiniteStateMachine:
    (String, Seq[StateTr], String, Seq[StateTr]) => Pure.FiniteStateMachine =
      (name, s1, ini, s2) =>
        Pure.FiniteStateMachine (
          name, (s1++s2).map (_._1).toList, (s1++s2).toMap, ini)

  val StateTransitions: (String, Option[Seq[Pure.Transition]]) => StateTr =
      _ -> _.getOrElse (Nil).toList

  val Transition: (String, Option[String], String) => Pure.Transition =
    (input, output, target) =>
      Pure.Transition (target, input, output getOrElse "")
}
