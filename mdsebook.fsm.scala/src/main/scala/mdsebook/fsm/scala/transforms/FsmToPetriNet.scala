// (c) mdsebook, wasowski, tberger
// An in-place transformation implemented directly in Scala (no special
// frameworks).  Translates a finite state machine to a Petri net.  There is
// presently no runner for this example.

package mdsebook.fsm.scala.transforms

import scala.jdk.CollectionConverters._
import mdsebook.scala.EMFScala._
import mdsebook.fsm
import mdsebook.{ petrinet => PN }

object FsmToPetriNet extends CopyingTrafo[fsm.Model, PN.Model] {

  private val pFactory = PN.PetrinetFactory.eINSTANCE

  private def convertState (s: fsm.State): PN.Place = pFactory.createPlace before {
    p => p setName s.getName
         p setTokenNo 0 }

  private def convertTransition (places: List[PN.Place]) (t: fsm.Transition): PN.Transition =
    pFactory.createTransition before { pnt =>
      pnt setInput t.getInput
      pnt.getFromPlace addAll (places.filter (_.getName == t.getSource.getName).asJava)
      pnt.getToPlace   addAll (places.filter (_.getName == t.getTarget.getName).asJava)
    }

  private def convertStateMachine (in: fsm.FiniteStateMachine): PN.PetriNet =
    pFactory.createPetriNet before { pn =>

      pn setName in.getName
      val places: List[PN.Place] = in.getStates.asScala.toList map convertState
      pn.getPlaces.addAll (places.asJava)
      pn.getTransitions.addAll (
        in.getStates.asScala.toList
        .flatMap (_.getLeavingTransitions.asScala.toList)
        .map (convertTransition (places) _)
        .asJava)

      // add a token to the place created from initial state
      places find (_.getName == in.getInitial.getName) foreach (_ setTokenNo 1)

      // for each end state generate a transition that throws a token away
      for (s <- fsmEndStates (in)) {
        val Some (p: PN.Place) = places find (_.getName == s.getName)
        pFactory.createTransition before { t =>
          t.getFromPlace.add (p)
          t setInput ""
          pn.getTransitions.add (t)
          p.getOutgoingTransitions.add (t) }
      }
    }

  override def run (in: fsm.Model): PN.Model =
    pFactory.createModel before {
      _.getPetrinets addAll (in.getMachines.asScala.map(convertStateMachine _).asJava) }

  private def fsmEndStates (m: fsm.FiniteStateMachine): List[fsm.State] =
    m.getStates.asScala.toList filter (_.getLeavingTransitions.isEmpty)

}
