// (c) mdsebook, wasowski, tberger
//
// Run using 'sbt test'.  The output file is placed in
// 'test-out/CoffeeMachine.java'
//
// We use a minimal subset of Java, and little structure.  Almost the same
// code should be generated for C, to be used on a micro-controller (for example).
//
// Lack of control-string inversion like in template languages gets in the way
// here and there.

// generator of dot files
// compile the results with
// dot -Tpdf CoffeeMachine.dot -o CoffeeMachine.pdf
package mdsebook.fsm.scala.transforms

import scala.jdk.CollectionConverters._

import mdsebook.fsm._
import mdsebook.scala.EMFScala._

object Fsm2Dot
{

  def fmtTransitionDot (t: Transition): String =
    s"""|  "${t.getSource.getName}" -> "${t.getTarget.getName}"
        |      [label="${t.getInput} / ${t.getOutput}"];"""

  def fmtStateDot (s: State): String =
    (s.getLeavingTransitions.asScala.map { fmtTransitionDot (_) } mkString "\n")

  def compileToDot (it: FiniteStateMachine): String =
    s"""digraph "${it.getName}" {
       |  _init -> "${it.getInitial.getName}";
          ${it.getStates.asScala.map { fmtStateDot (_) }  mkString "\n"}
       |  ${it.getInitial.getName} [shape=doublecircle];
       |  _init [shape=point];
       |}
       |""".stripMargin

}
