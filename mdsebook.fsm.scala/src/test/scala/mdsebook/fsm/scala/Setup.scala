// (c) mddbook, wasowski, tberger
// Helper methods to load test-cases from mdsebook.fsm
package mdsebook.fsm.scala

object Setup {

  import mdsebook.fsm.FsmPackage
  import org.eclipse.emf.ecore.resource.Resource
  import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
  import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
  import org.eclipse.emf.common.util.URI

  import mdsebook.fsm.FsmPackage
  import mdsebook.fsm.Model

  Resource.Factory.Registry.INSTANCE.
    getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)
  FsmPackage.eINSTANCE.eClass

  // Load a test case
  def loadFSM (name: String): Model = {
    val uri = URI.createURI (s"../mdsebook.fsm/test-files/${name}.xmi")
    val res = (new ResourceSetImpl).getResource (uri, true)
    res.getContents.get(0).asInstanceOf[Model]
  }

}
