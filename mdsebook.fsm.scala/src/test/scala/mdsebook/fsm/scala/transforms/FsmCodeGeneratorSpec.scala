// (c) mddbook, wasowski, tberger
package mdsebook.fsm.scala.transforms

import org.eclipse.emf.common.util.Diagnostic
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.util.Diagnostician
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import java.io.PrintWriter

import mdsebook.scala.EMFScala._
import mdsebook.fsm
import mdsebook.fsm.scala.transforms._

class FsmCodeGeneratorSpec extends AnyFreeSpec with Matchers {

  fsm.FsmPackage.eINSTANCE.eClass
  val m :fsm.Model = loadFromXMI ("../mdsebook.fsm/test-files/CoffeeMachine.xmi")

  "FsmToJava" - {

    "just run the trafo for manual inspection"  in {
      val java = Fsm2Java compileToJava m.getMachines.get(0)
      new PrintWriter("test-out/CoffeeMachine.java") { write(java); close }

      val dot = Fsm2Dot compileToDot m.getMachines.get(0)
      new PrintWriter("test-out/CoffeeMachine.dot") { write(dot); close }
    }

  }

}
