// (c) mddbook, wasowski, tberger
// These tests cannot depend on xtext parser, as we land a circular dependency
// then.  This is probably possible to sort-out but better avoided.
package mdsebook.fsm.scala

import scala.jdk.CollectionConverters._

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.Diagnostician
import org.eclipse.emf.common.util.Diagnostic

class ConstraintsSpec extends AnyFreeSpec with Matchers {
  
  import mdsebook.fsm.scala.Setup.loadFSM
  import mdsebook.fsm._
  import Constraints._

  // When you validate static semantics with Ecore based languages, you should 
  // first validate meta-model constraints.  This is not done automatically at
  // loading time.
  "Test meta-model constraints on our input cases" - {

    // A helper call to EMF/Ecore validation
    def validate (eObject: EObject): Int = {
      val diagnostic = Diagnostician.INSTANCE.validate (eObject)

      if (diagnostic.getSeverity != Diagnostic.OK)
        for { i <- diagnostic.getChildren.asScala }
        yield { info ("\t" + i.getMessage) }

      diagnostic.getSeverity
    }



    "positive tests" - {
  
      "test-00" in { validate (loadFSM ("test-00")) should be (Diagnostic.OK) }

      "test-01" in { validate (loadFSM ("test-01")) should be (Diagnostic.OK) }

      "test-02" in { validate (loadFSM ("test-02")) should be (Diagnostic.OK) }

      "test-03" in { validate (loadFSM ("test-03")) should be (Diagnostic.OK) }

      "test-05" in { validate (loadFSM ("test-05")) should be (Diagnostic.OK) }

      "test-06" in { validate (loadFSM ("test-06")) should be (Diagnostic.OK) }

      "test-07" in { validate (loadFSM ("test-07")) should be (Diagnostic.OK) }

      "test-10" in { validate (loadFSM ("test-10")) should be (Diagnostic.OK) }

      "CoffeeMachine" in { 
        validate (loadFSM ("CoffeeMachine")) should be (Diagnostic.OK) }

    }



    "negative tests" - {

      "missing initial and names (test-04)" in { 
        validate (loadFSM ("test-04")) should be (Diagnostic.ERROR) }

      "name conflicts (test-08)" in { 
        validate (loadFSM ("test-08")) should be (Diagnostic.ERROR) }

      "name conflicts (test-09)" in { 
        validate (loadFSM ("test-09")) should be (Diagnostic.ERROR) }
    }

  }



  // Tests for constraints from the chapter

  "C1" - {

    "positive tests" - { 

      "single machine (test-00)" in { 
        C1.checkAll (loadFSM ("test-00")) should be (true) }

      "two machines (test-01.xmi)" in { 
        C1.checkAll (loadFSM ("test-01")) should be (true) }

      "two larger machines (test-02)" in { 
        C1.checkAll (loadFSM ("test-02")) should be (true) }

      "empty model (test-07)" in { 
        C1.checkAll (loadFSM ("test-07")) should be (true) }

    }

    "negative tests" - {

      "two machines (test-08)" in { 
        C1.checkAll (loadFSM ("test-08")) should be (false) }

      "five machines (test-09)" in { 
        C1.checkAll (loadFSM ("test-09")) should be (false) }

    }
  }




  "C2" - {

    "positive tests" - {

      "single machine (test-00)" in { 
        C2.checkAll (loadFSM ("test-00")) should be (true) }

      "two machines (test-01.xmi)" in { 
        C2.checkAll (loadFSM ("test-01")) should be (true) }

      "two larger machines (test-02)" in { 
        C2.checkAll (loadFSM ("test-02")) should be (true) }

      "empty model (test-07)" in { 
        C2.checkAll (loadFSM ("test-07")) should be (true) }

      "five machines with repeated state names (test-09)" in { 
        C2.checkAll (loadFSM ("test-09")) should be (true) }

      "empty state collection (test-11)" in { 
        C2.checkAll (loadFSM ("test-11")) should be (true) 
      }

      "CoffeeMachine.xmi" in { 
        C2.checkAll (loadFSM ("CoffeeMachine")) should be (true) }
    }

    "negative tests" - {

      "two no-name states (test-04)" in { 
        C2.checkAll (loadFSM ("test-04")) should be (false) 
      }

      "one machine (test-14)" in { 
        C2.checkAll (loadFSM ("test-14")) should be (false) }

      "two machines (test-08)" in { 
        C2.checkAll (loadFSM ("test-08")) should be (false) }
    }

  }




  "C3" - {

    "positive tests" - {

      "single machine (test-00)" in { 
        C3.checkAll (loadFSM ("test-00")) should be (true) }

      "two machines (test-01.xmi)" in { 
        C3.checkAll (loadFSM ("test-01")) should be (true) }

      "two larger machines (test-03)" in { 
        C3.checkAll (loadFSM ("test-03")) should be (true) }

    }

    "negative tests" - {

      "Example from Fig. 5.3 (test-10)" in { 
        C3.checkAll (loadFSM ("test-10")) should be (false)
      }

    }

  }



  "C4" - {

    "positive tests" - {

      "single machine (test-00)" in { 
        C4.checkAll (loadFSM ("test-00")) should be (true) }

      "two machines (test-01.xmi)" in { 
        C4.checkAll (loadFSM ("test-01")) should be (true) }

      "two larger machines with transitions (test-05)" in { 
        C4.checkAll (loadFSM ("test-05")) should be (true) }

    }

    "negative tests" - {

      "Cross machine transition (test-12)" in { 
        C4.checkAll (loadFSM ("test-12")) should be (false)
      }
    }
  }



  "C5" - {

    "positive tests" - {

      "single machine (test-00)" in { 
        C5.checkAll (loadFSM ("test-00")) should be (true) }

      "two machines (test-01.xmi)" in { 
        C5.checkAll (loadFSM ("test-01")) should be (true) }

      "two larger machines with transitions (test-05)" in { 
        C5.checkAll (loadFSM ("test-05")) should be (true) }

      "CoffeeMachine.xmi" in { 
        C5.checkAll (loadFSM ("CoffeeMachine")) should be (true) }

    }

    "negative tests" - {

      "Cross machine transition (test-12)" in { 
        C5.checkAll (loadFSM ("test-12")) should be (false)
      }

      "Valid but with dead state (test-13)" in { 
        C5.checkAll (loadFSM ("test-13")) should be (false)
      }

    }
  }



  "C6" - {

    "positive tests" - {

      "single machine (test-00)" in { 
        C6.checkAll (loadFSM ("test-00")) should be (true) }

      "two machines (test-01.xmi)" in { 
        C6.checkAll (loadFSM ("test-01")) should be (true) }

      "two larger machines with transitions (test-05)" in { 
        C6.checkAll (loadFSM ("test-05")) should be (true) }

      "CoffeeMachine.xmi" in { 
        C6.checkAll (loadFSM ("CoffeeMachine")) should be (true) }

    }

    "negative tests" - {

      "Valid but non-deterministic (test-13)" in { 
        C6.checkAll (loadFSM ("test-13")) should be (false)
      }

    }
  }

}
