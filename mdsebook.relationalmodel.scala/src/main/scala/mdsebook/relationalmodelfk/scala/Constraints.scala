// (c) mdsebook, wasowski, tberger

package mdsebook.relationalmodelfk.scala

import scala.collection.JavaConverters._
import mdsebook.scala.EMFScala._
import mdsebook.relationalmodelfk._

object Constraints {

  val invariants: List[Constraint] = List (

    inv[Root] { _ => true } // modify this constraint

  )

}
