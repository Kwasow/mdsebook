// (c) mdsebook, wasowski, berger
grammar mdsebook.fsm.xtext.Fsm with org.eclipse.xtext.common.Terminals

import "http://www.mdsebook.org/mdsebook.fsm"
import "http://www.eclipse.org/emf/2002/Ecore" as ecore

model returns Model:
  {Model} machines+=machineBlock*;

machineBlock returns FiniteStateMachine:
  {FiniteStateMachine}
  'machine' name=EString ('['
      ( (states+=stateBlock)+
        & ('initial' initial=[State])
        & (states+=stateBlock)* )?
  ']')?;

stateBlock returns State:
   {State}
   'state' name=EString
   ('[' leavingTransitions+=transition* ']')?;

transition returns Transition:
  'on' 'input'? input=EString         // inputClause
  ('output' output=EString 'and')?    // outputClause
  'go' 'to' target=[State|EString];   // targetClause

EString returns ecore::EString:
  STRING | ID;

// The above nonterminal must be called "EString" as then
// a value converter picks up and strips quotes of the string.
// Implemented here: https://github.com/eclipse/xtext-core/blob/master/org.eclipse.xtext/src/org/eclipse/xtext/common/services/Ecore2XtextTerminalConverters.java
// So even though EString is not a standard terminal in Xtext (it is generated
// for Ecore models in the grammar, can be removed, renamed, etc.), it does
// have special semantics. For a moment, I was worried that Xtext grammar
// language is not oblivious to alpha-conversions, until I found this sneaky
// value converter.
